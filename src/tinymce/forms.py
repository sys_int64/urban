from django import forms
from django.db import models
from django.conf import settings
from django.template import loader


class TinyMCEInput(forms.Widget):
    class Media:
        js = (
            '//cdn.tinymce.com/4/tinymce.min.js',
            'admin/js/tinymce-widget.js',
        )

    def render(self, name, value, attrs=None):
        if value is None:
            value = ""

        context = {
            "name": name,
            "value": value,
            "STATIC_URL": settings.STATIC_URL,
        }

        template = loader.get_template('admin/tinymce-widget.html')
        return template.render(context)


class TinyMCEField(models.TextField):
    description = "TinyMCEField"

    def formfield(self, **kwargs):
        kwargs.update({'widget': TinyMCEInput})
        return super().formfield(**kwargs)
