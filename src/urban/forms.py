from django import forms
from django.utils.translation import ugettext_lazy as _


class ContactForm(forms.Form):
    name = forms.CharField(
        max_length=60,
        widget=forms.TextInput(attrs={"placeholder": _("Имя")})
    )
    contacts = forms.CharField(
        max_length=60,
        widget=forms.TextInput(attrs={"placeholder": _("Контакты")})
    )
    content = forms.CharField(
        widget = forms.Textarea(attrs={"placeholder": _("Ваш вопрос"), "rows": "5"})
    )
