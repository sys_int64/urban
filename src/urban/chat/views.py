from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from urban.chat.serializers import JoinSerializer
from urban.views import main_context


def admin_chat(request):

    context = main_context(request)
    return render_to_response("admin_chat.html", context=context)


@api_view(["POST"])
def join(request):
    serializer = JoinSerializer(request.data)
    if not serializer.is_valid():
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if not request.session.exists(request.session.session_key):
        request.session.create()

    serializer.save(owner=request.session.session_key)
    return Response(status=status.HTTP_200_OK)


@api_view(["POST"])
def send(request):
    return Response(status=status.HTTP_200_OK)


@api_view(["GET"])
def get_history(request):
    data = {
        "needJoin": True,
        "joinForm": render_to_string("chat/greet_form.html"),
    }
    return Response(data, status=status.HTTP_200_OK)
