from rest_framework import serializers

from urban.chat.models import Dialog


class JoinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dialog
        fields = (
            "name"
        )
