from django.contrib.auth.models import User
from django.db import models


class Dialog(models.Model):
    owner = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    admin = models.ForeignKey(User, related_name="admin", null=True)


class Message(models.Model):
    from_user = models.ForeignKey(User, related_name="from_user")
    to_user = models.ForeignKey(User, related_name="to_user")
    text = models.TextField()
    datetime = models.DateTimeField()
