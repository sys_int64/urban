from django.conf.urls import url
from urban.chat import views

urlpatterns = [
    url(r'^admin/$', views.admin_chat, name="admin"),
    url(r'^getHistory/$', views.get_history, name="join"),
    url(r'^join/$', views.join, name="join"),
    url(r'^send/$', views.send, name="send"),
]
