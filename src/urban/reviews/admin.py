from django.contrib import admin
from django import forms

from urban.preferences.admin import SingleLanguageInline
from urban.preferences.forms import LanguagesSelectInput, LanguageInput
from urban.preferences.models import Language
from urban.reviews.models import Review, ReviewTranslation


class ReviewsAdminForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = '__all__'

    lang = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                  required=False, widget=LanguagesSelectInput)


class ReviewAdminTranslationForm(forms.ModelForm):
    class Meta:
        model = ReviewTranslation
        fields = '__all__'

    language = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                      required=True, widget=LanguageInput)


class TranslationInline(SingleLanguageInline):
    model = ReviewTranslation
    extra = 0
    form  = ReviewAdminTranslationForm
    max_num = 999


class ReviewsAdmin(admin.ModelAdmin):
    inlines = [TranslationInline]
    form = ReviewsAdminForm


admin.site.register(Review, ReviewsAdmin)
