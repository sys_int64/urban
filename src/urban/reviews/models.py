from django.db import models

from urban.preferences.models import Language, LanguageModelManager


class Review(models.Model):
    class Meta:
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"

    photo = models.ImageField()
    objects = LanguageModelManager("review")

    def translation(self):
        return self.translations.all().filter(language=Language.current).first()

    def __str__(self):
        tset = self.translations.all()
        translation = tset.filter(language__country_code="ru").first()
        translation = translation or tset.first()

        if translation is not None:
            return translation.name
        else:
            return self.pk


class ReviewTranslation(models.Model):
    class Meta:
        default_related_name = "translations"

    review = models.ForeignKey(Review)
    language = models.ForeignKey(Language, verbose_name="Язык", null=True, blank=True)

    name = models.CharField(max_length=60)
    text = models.TextField()
