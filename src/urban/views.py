from django.core.mail import EmailMessage
from django.shortcuts import render_to_response, get_object_or_404
from django.core.urlresolvers import resolve
from django.template.context_processors import csrf
from django.template.loader import get_template
from django.utils.translation import LANGUAGE_SESSION_KEY

from urban.forms import ContactForm
from urban.preferences.models import Preferences, PreferencesTranslation, Language
from urban.reviews.models import Review
from urban.services.models import Category, Service
from django.utils import translation


def main_context(request, lang_code=None, context=None):
    if context is None:
        context = {}

    lang_code = lang_code or "ru"
    translation.activate(lang_code)
    contact_form = ContactForm()

    context.update({
        "request": request,
        "categories": Category.objects.all(),
        "current_url_name": resolve(request.path_info).url_name,
        "preferences": Preferences.objects.all().first(),
        "contact_form": contact_form,
        "languages": Language.objects.all_for_page(request)
    })
    context.update(csrf(request))

    return context


def layout(request, template):
    return render_to_response("layout/{}.html".format(template), context=main_context(request))


# Sorry, I'm lazy man
def handle_contact_form(request):
    if request.method != "POST":
        return {"contact_form_fail": False, "contact_form_success": False}

    contact_form = ContactForm(data=request.POST, auto_id=False)

    if not contact_form.is_valid():
        return {"contact_form_fail": True, "contact_form_success": False}

    template = get_template("contact_template.txt")
    context = {
        "name": contact_form.cleaned_data["name"],
        "contacts": contact_form.cleaned_data["contacts"],
        "content": contact_form.cleaned_data["content"],
    }
    content = template.render(context)
    preferences = Preferences.objects.all().first()
    email = EmailMessage(
        "New contact form submission",
        content,
        "Urban",
        [preferences.send_email]
    )
    email.send()
    return {"contact_form_fail": False, "contact_form_success": True}


def index(request, lang_code=None):
    context = main_context(request, lang_code)
    context.update({
        "reviews": Review.objects.all(),
        "popular_services": Service.objects.filter(is_popular=True)[:3],
    })
    context.update(handle_contact_form(request))
    return render_to_response("index.html", context=context)


def services(request, lang_code=None, slug=""):
    category = get_object_or_404(Category, slug=slug)
    context = main_context(request, lang_code)
    context.update({
        "category": category,
        "services": Service.objects.filter(category=category),
    })
    context.update(handle_contact_form(request))
    return render_to_response("services.html", context=context)
