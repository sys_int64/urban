from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static

from urban import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', views.index, name="index"),
    url(r'^(?P<lang_code>[a-z]+)/$', views.index, name="index"),
    url(r'^services/(?P<slug>[A-z0-9_\-.]+)/$', views.services, name="services"),
    url(r'^(?P<lang_code>[a-z]+)/services/(?P<slug>[A-z0-9_\-.]+)/$', views.services, name="services"),
    url(r'^chat/', include('urban.chat.urls', namespace="chat")),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]


if settings.DEBUG:
    urlpatterns += [
        url(r'^layout/(?P<template>[A-z0-9_\-.]+)/$', views.layout),
    ]

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
