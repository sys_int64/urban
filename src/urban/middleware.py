from django.utils.deprecation import MiddlewareMixin

from urban.preferences.models import Language


class LanguageMiddleware(MiddlewareMixin):
    def process_request(self, request):
        lang_code = request.path.lstrip("/")[:2]
        Language.current = Language.objects.filter(country_code=lang_code).first()
        Language.current = Language.current or Language.objects.filter(country_code="ru").first()
        request.language = Language.current
