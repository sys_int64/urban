from django.apps import AppConfig


class ServicesConfig(AppConfig):
    name = 'urban.services'
    verbose_name = "Услуги"
