# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-05-16 13:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('preferences', '0004_auto_20170516_1950'),
        ('services', '0002_service_is_popular'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceTranslation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60)),
                ('price', models.CharField(max_length=20)),
                ('language', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='service_lang', to='preferences.Language', verbose_name='Язык')),
            ],
            options={
                'default_related_name': 'translations',
            },
        ),
        migrations.RemoveField(
            model_name='service',
            name='name',
        ),
        migrations.RemoveField(
            model_name='service',
            name='price',
        ),
        migrations.AddField(
            model_name='servicetranslation',
            name='service',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='translations', to='services.Service'),
        ),
    ]
