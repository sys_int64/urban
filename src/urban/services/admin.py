from django import forms
from django.contrib import admin
from django.utils.html import format_html

from urban.preferences.admin import SingleLanguageInline, LanguageListDisplayMixin
from urban.preferences.forms import LanguagesSelectInput, LanguageInput
from urban.preferences.models import Language
from urban.services.models import Category, ServiceTranslation, CategoryTranslation
from urban.services.models import Service


# Service category
class CategoryAdminForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'

    lang = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                  required=False, widget=LanguagesSelectInput)


class CategoryAdminTranslationForm(forms.ModelForm):
    class Meta:
        model = CategoryTranslation
        fields = '__all__'

    language = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                      required=True, widget=LanguageInput)


class CategoryTranslationInline(SingleLanguageInline):
    model = CategoryTranslation
    extra = 0
    form = CategoryAdminTranslationForm
    max_num = 999


class CategoryAdmin(LanguageListDisplayMixin, admin.ModelAdmin):
    inlines = [CategoryTranslationInline]
    form = CategoryAdminForm
    list_display = "__str__", "languages"


# Service
class ServiceAdminForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = '__all__'

    lang = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                  required=False, widget=LanguagesSelectInput)


class ServiceAdminTranslationForm(forms.ModelForm):
    class Meta:
        model = ServiceTranslation
        fields = '__all__'

    language = forms.ModelChoiceField(Language.objects.all(), label="Язык",
                                      required=True, widget=LanguageInput)


class TranslationInline(SingleLanguageInline):
    model = ServiceTranslation
    extra = 0
    form  = ServiceAdminTranslationForm
    max_num = 999


class ServiceAdmin(LanguageListDisplayMixin, admin.ModelAdmin):
    list_filter = ('category', 'is_popular')
    inlines = [TranslationInline]
    form = ServiceAdminForm
    list_display = "__str__", "languages"


admin.site.register(Category, CategoryAdmin)
admin.site.register(Service, ServiceAdmin)
