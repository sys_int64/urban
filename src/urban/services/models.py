from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.urls import reverse

from tinymce.forms import TinyMCEField
from urban.preferences.models import Language, LanguageModelManager
from django.utils.translation import ugettext as _


class Category(models.Model):
    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    slug = models.CharField(max_length=60)
    objects = LanguageModelManager("category")

    def translation(self):
        return self.translations.all().filter(language=Language.current).first()

    def permalink(self):
        if Language.current.country_code == "ru":
            return reverse("services", kwargs={"slug": self.slug})

        return reverse("services", kwargs={"slug": self.slug, "lang_code": Language.current.country_code})

    def __str__(self):
        tset = self.translations.all()
        translation = tset.filter(language__country_code="ru").first()
        translation = translation or tset.first()

        if translation is not None:
            return translation.name
        else:
            return self.pk


class CategoryTranslation(models.Model):
    class Meta:
        default_related_name = "translations"

    category = models.ForeignKey(Category)
    language = models.ForeignKey(Language, verbose_name="Язык", related_name="service_category_lang",
                                 null=True, blank=True)

    name = models.CharField(max_length=60)


class Service(models.Model):
    class Meta:
        verbose_name = _("Услуга")
        verbose_name_plural = "Услуги"

    category = models.ForeignKey(Category)
    is_popular = models.BooleanField(default=False)

    objects = LanguageModelManager("service")

    def translation(self):
        return self.translations.all().filter(language=Language.current).first()

    def __str__(self):
        tset = self.translations.all()
        translation = tset.filter(language__country_code="ru").first()
        translation = translation or tset.first()

        if translation is not None:
            return translation.name
        else:
            return self.pk


class ServiceTranslation(models.Model):
    class Meta:
        default_related_name = "translations"

    service = models.ForeignKey(Service)
    language = models.ForeignKey(Language, verbose_name="Язык",
                                 related_name="service_lang", null=True, blank=True)

    name = models.CharField(max_length=60)
    price = models.CharField(max_length=20)
    details = RichTextUploadingField(default="<p>No details</p>")
