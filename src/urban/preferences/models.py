from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

from tinymce.forms import TinyMCEField


class LanguageModelManager(models.Manager):
    def __init__(self, name):
        super().__init__()
        self.name = name

    def get_queryset(self):
        query = {
            "{}translation__language".format(self.name): Language.current
        }
        return super().get_queryset().filter(**query)


class Languages(models.Model):
    class Meta:
        verbose_name = 'Язык'
        verbose_name_plural = 'Языки'


class LanguageManager(models.Manager):
    def all_for_page(self, request):
        languages = []
        language_codes = self.values_list("country_code", flat=True)

        for language in self.all():
            filtered_path = request.path

            if filtered_path[1:3] in language_codes:
                filtered_path = filtered_path[3:]

            if language.country_code == "ru":
                language.permalink = filtered_path
            else:
                language.permalink = "/%s%s" % (language.country_code, filtered_path)

            languages.append(language)

        return languages


class Language(models.Model):
    current = None

    languages = models.ForeignKey(Languages)
    country_code = models.CharField("Код страны", max_length=4, primary_key=True)
    country_name = models.CharField("Название", max_length=255, default="")

    objects = LanguageManager()

    def __str__(self):
        return self.country_name

    @property
    def active(self):
        if Language.current is None:
            return False

        return Language.current.country_code == self.country_code


class Preferences(models.Model):
    send_email = models.EmailField(verbose_name="Contact form email", blank=True)
    vk_url = models.URLField(verbose_name="VK")
    fb_url = models.URLField(verbose_name="Facebook")
    tw_url = models.URLField(verbose_name="Twitter")

    def translation(self):
        return self.preferencestranslation_set.all().filter(language=Language.current).first()


class PreferencesTranslation(models.Model):
    preferences = models.ForeignKey(Preferences)
    language = models.ForeignKey(Language, verbose_name="Язык", null=True, blank=True)

    info_phone = models.CharField(max_length=60, verbose_name="Информационный телефон")
    master_phone = models.CharField(max_length=60, verbose_name="Телефон мастера")
    form_email = models.EmailField(verbose_name="E-mail для формы")
    about_company = RichTextUploadingField()
