(function($) {
    var tmce_field;
    var oldCallback = null;

    $(document).ready(function() {
        tinymce.init({
            selector: '.tinymce-widget',
            plugins: [
              'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
              'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media imagetools nonbreaking',
              'save table contextmenu directionality emoticons template paste textcolor'
            ],

            height: 400
        });
    });
})(django.jQuery);
