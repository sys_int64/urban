function sendRequest(url, method, data, $messagesContainer, callback) {
    $.ajax({
        url: url,
        data: data,
        method: method,
        dataType: "json",
        success: function(data) {
            callback(data);
        },
        error: function(info) {
            $messagesContainer.html("");
            $messagesContainer.addClass("error");

            if (info.status >= 500) {
                $messagesContainer.append("<li>Произошла ошибка на сервере</li>");
                return;
            }

            // Bad request
            var json = $.parseJSON(info.responseText);
            $.each(json.errors, function () {
                $messagesContainer.append("<li>" + this.desc + "</li>");
            });
        }
    });
}

(function($) {
    const $fullpage = $("#fullpage");

    function update_sizes() {
        const $row = $("#reviews").find(".row");
        var height = Math.floor($row.outerHeight() / 2);
        $row.css("margin-top", -height);
    }

    function auto_height() {
        $(".auto-height").each(function () {
            if ($(this).css("position") !== "absolute")
                return;

            $(this).find(".left").height(0);
            var height = $(this).outerHeight();
            $(this).css("margin-top", Math.floor(-height / 2));
            $(this).css("min-height", "280px");
            $(this).find(".left").height(height);
        });
    }

    $(document).ready(function () {
        // const $contactModal = $("#contact-modal");

        $("#reviews").find(".items").slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            fade: true
        });

        if (!$fullpage.hasClass("services")) {
            $fullpage.fullpage({
                verticalCentered: false,
                css3: false
            });
        }

        $("#header").find("button.down").click(function() {
            $('#fullpage').fullpage.moveSectionDown();
        });

        update_sizes();
        auto_height();

        setInterval(function () {
            update_sizes();
            auto_height();
        }, 1000);

        $(".open-modal").click(function () {
            const $modal = $("#"+$(this).data("modal"));
            $modal.css("display", "block");
            $modal.animate({"opacity": 1}, 200, "linear");
        });

        $(".modal").find(".close").click(function () {
            const $modal = $(this).closest(".modal");
            $modal.animate({"opacity": 0}, 200, "linear", function () {
                $modal.css("display", "none");
            });
        });

        $("#online-chat").find(".toolbar .menu-button").click(function () {
            $("#online-chat").find(".toolbar-menu").toggle();
        });

        $("#mobile-menu-link").click(function(event) {
            $("#mobile-menu").show();
            event.preventDefault();
        });

        $("#mobile-menu-close").click(function(event) {
            $("#mobile-menu").hide();
            event.preventDefault();
        });
    });
})(jQuery);
