(function($) {
    var $messagesContainer;
    var $chatContainer;
    var $chatMessagesContainer;

    function _message(text, messageClass) {
        $messagesContainer.html("");
        $messagesContainer.removeClass("error");
        $messagesContainer.removeClass("success");
        $messagesContainer.addClass(messageClass);
        $messagesContainer.append("<li>" + text + "</li>");
    }

    function error(text) {
        _message(text, "error");
    }

    function success(text) {
        _message(text, "success");
    }

    function sendPost(url, data, callback) {
        sendRequest(url, "POST", data, $messagesContainer, callback);
    }

    function sendGet(url, data, callback) {
        sendRequest(url, "GET", data, $messagesContainer, callback);
    }

// -------------------------------------------------------------------------------------------------

    function renderJoinForm(data) {
        $chatMessagesContainer.html("");
        $chatMessagesContainer.append(data.joinForm);
        var $form = $chatMessagesContainer.find();


    }

    function renderHistory(history) {
    }

    function load() {
        sendGet("/chat/getHistory/", {}, function(data) {
            if (data.needJoin) {
                renderJoinForm(data);
            } else {
                renderHistory(data.messages);
            }
        });
    }

    $(document).ready(function() {
        $chatContainer = $("#online-chat");
        $messagesContainer = $chatContainer.find(".info-messages");
        $chatMessagesContainer = $chatContainer.find(".messages");
        load();
    });
})(jQuery);
